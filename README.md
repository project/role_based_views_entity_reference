Role Based Views Entity Reference
=================================


CONTENTS
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

 * This module extendeds functionality of Views entity reference and can handle situations when you want to add one entity reference field with different data filled in according to each user role and control access to data.

 * i.e. if you want a user that has an admin role to select between all other admin users and non-admin users to select other users with specific roles.

 * This idea can be generalized to any entity and not only to user entity. you just need to select "Views: Filter by an entity reference role based views plugin" option while you are creating or editing the entity reference field and Voila! here you go.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/role_based_views_entity_reference

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/role_based_views_entity_reference 

REQUIREMENTS
------------

 * This module requires no modules outside of Drupal core.


INSTALLATION
------------

* to get the module you can download and unzip it in contrib modules folder or better use composer to install the module and manage your dependencies:
```
 $ composer require drupal/role_based_views_entity_reference.
```
* visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * You need to create an entity reference field on any entity and choose Views: Filter by an entity reference role based views plugin, and you can select a view entity reference display to use for each role. it's dynamic and can have views selected as many as you have roles in system.

