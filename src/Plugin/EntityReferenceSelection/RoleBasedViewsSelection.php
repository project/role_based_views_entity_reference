<?php

namespace Drupal\role_based_views_entity_reference\Plugin\EntityReferenceSelection;

use Drupal\views\Plugin\EntityReferenceSelection\ViewsSelection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Views;

// New views handler for entity selection plugin to show view result based on role.

/**
 * Plugin implementation of the 'selection' entity_reference.
 *
 * @EntityReferenceSelection(
 *   id = "role_based_views_selection",
 *   label = @Translation("Views: Filter by an entity reference role based views plugin"),
 *   group = "role_based_views_selection",
 *   weight = 0
 * )
 */
class RoleBasedViewsSelection extends ViewsSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $view_settings = $this->getConfiguration()['view'];
    $displays = Views::getApplicableViews('entity_reference_display');
    // Filter views that list the entity type we want, and group the separate
    // displays by view.
    $entity_type = $this->entityTypeManager->getDefinition($this->configuration['target_type']);
    $entity_base_table = $entity_type->getBaseTable();
    $entity_data_table = $entity_type->getDataTable();

    $view_storage = $this->entityTypeManager->getStorage('view');
    $views_options = [];
    foreach ($displays as $data) {
      list($view_id, $display_id) = $data;
      /** @var  Drupal\views\ViewExecutable */
      $view = $view_storage->load($view_id);
      $view_base_table = $view->get('base_table');
      if (in_array($view_base_table, [$entity_base_table, $entity_data_table])) {
        $display = $view->get('display');
        $views_options[$view_id . ':' . $display_id] = $view_id . ' - ' . $display[$display_id]['display_title'];
      }
    }

    // The value of the 'view_and_display' select below will need to be split
    // into 'view_name' and 'view_display' in the final submitted values, so
    // we massage the data at validate time on the wrapping element (not
    // ideal).
    $form['view']['#element_validate'] = [
      [get_called_class(),
        'settingsFormValidate',
      ],
    ];

    if ($views_options) {
      array_unshift($views_options, 'none');
      $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
      ;

      foreach ($roles as $role_machine => $role_obj) {
        $default = !empty($view_settings['view_name_' . $role_machine]) ? $view_settings['view_name_' . $role_machine] . ':' . $view_settings['display_name_' . $role_machine] : NULL;
        $form['view']['view_and_display_' . $role_machine] = [
          '#type' => 'select',
          '#title' => $this->t('View used to select the entities for @Role', ['@Role' => $role_obj->label()]),
          '#required' => FALSE,
          '#options' => $views_options,
          '#default_value' => $default,
          '#description' => '<p>' . $this->t('Choose the view and display that select the entities that can be referenced for @Role.<br />Only views with a display of type "Entity Reference" are eligible.', ['@Role' => $role_obj->label()]) . '</p>',
        ];

        $default = !empty($view_settings['arguments_' . $role_machine]) ? implode(', ', $view_settings['arguments_' . $role_machine]) : '';
        $form['view']['arguments_' . $role_machine] = [
          '#type' => 'textfield',
          '#title' => $this->t('View arguments for @Role', ['@Role' => $role_obj->label()]),
          '#default_value' => $default,
          '#required' => FALSE,
          '#description' => $this->t('Provide a comma separated list of arguments to pass to the view.'),
        ];
      }

    }
    else {

      if ($this->currentUser->hasPermission('administer views') && $this->moduleHandler->moduleExists('views_ui')) {
        $form['view']['no_view_help'] = [
          '#markup' => '<p>' . $this->t('No eligible views were found. <a href=":create">Create a view</a> with an <em>Entity Reference</em> display, or add such a display to an <a href=":existing">existing view</a>.', [
            ':create' => Url::fromRoute('views_ui.add')->toString(),
            ':existing' => Url::fromRoute('entity.view.collection')->toString(),
          ]) . '</p>',
        ];
      }
      else {
        $form['view']['no_view_help']['#markup'] = '<p>' . $this->t('No eligible views were found.') . '</p>';
      }
    }
    return $form;
  }

  /**
   * Element validate; Check View is valid.
   *
   * User must select at least one view per role.
   */
  public static function settingsFormValidate($element, FormStateInterface $form_state, $form) {
    // Split view name and display name from the 'view_and_display' value.
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $values = [];
    foreach ($roles as $role_machine => $role_obj) {
      if (!empty($element['view_and_display_' . $role_machine]['#value'])) {
        list($view, $display) = explode(':', $element['view_and_display_' . $role_machine]['#value']);
        // Explode the 'arguments' string into an actual array. Beware, explode()
        // turns an empty string into an array with one empty string. We'll need an
        // empty array instead.
        $arguments_string = trim($element['arguments_' . $role_machine]['#value']);
        if ($arguments_string === '') {
          $arguments = [];
        }
        else {
          // array_map() is called to trim whitespaces from the arguments.
          $arguments = array_map('trim', explode(',', $arguments_string));
        }

        $values += [
          'view_name_' . $role_machine => $view,
          'display_name_' . $role_machine => $display,
          'arguments_' . $role_machine => $arguments,
        ];

      }
    }

    if (empty($values)) {
      $form_state->setError($element, t('The views entity selection mode requires a view at least for one role.'));
      return;
    }
    $form_state->setValueForElement($element, $values);
  }

  /**
   * Gets Highest/Last Role For Current User.
   */
  private function getHighestRoleForCurrentUser() {
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    $view_settings = $this->getConfiguration()['view'];
    // Select higher order role view if user has mutiple roles.
    $role = array_pop($roles);
    if (isset($view_settings['view_name_' . $role])) {
      return $role;
    }
    return '';
  }

  /**
   * Initializes a view.
   *
   * @param string|null $match
   *   (Optional) Text to match the label against. Defaults to NULL.
   * @param string $match_operator
   *   (Optional) The operation the matching should be done with. Defaults
   *   to "CONTAINS".
   * @param int $limit
   *   Limit the query to a given number of items. Defaults to 0, which
   *   indicates no limiting.
   * @param array|null $ids
   *   Array of entity IDs. Defaults to NULL.
   *
   * @return bool
   *   Return TRUE if the view was initialized, FALSE otherwise.
   */
  protected function initializeView($match = NULL, $match_operator = 'CONTAINS', $limit = 0, $ids = NULL) {
    $role = $this->getHighestRoleForCurrentUser();
    if (empty($role)) {
      return FALSE;
    }
    $view_name = $this->getConfiguration()['view']['view_name_' . $role];
    $display_name = $this->getConfiguration()['view']['display_name_' . $role];

    // Check that the view is valid and the display still exists.
    $this->view = Views::getView($view_name);
    if (!$this->view || !$this->view->access($display_name)) {
      \Drupal::messenger()->addWarning($this->t('The reference view %view_name cannot be found.', ['%view_name' => $view_name]));
      return FALSE;
    }
    $this->view->setDisplay($display_name);

    // Pass options to the display handler to make them available later.
    $entity_reference_options = [
      'match' => $match,
      'match_operator' => $match_operator,
      'limit' => $limit,
      'ids' => $ids,
    ];
    $this->view->displayHandlers->get($display_name)->setOption('entity_reference_options', $entity_reference_options);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $role = $this->getHighestRoleForCurrentUser();
    $return = [];
    if (empty($role)) {
      return $return;
    }
    
    $display_name = $this->getConfiguration()['view']['display_name_' . $role];
    $arguments = $this->getConfiguration()['view']['arguments_' . $role];
    $result = [];
    if ($this->initializeView($match, $match_operator, $limit)) {
      // Get the results.
      $result = $this->view->executeDisplay($display_name, $arguments);
    }

    if ($result) {
      foreach ($this->view->result as $row) {
        $entity = $row->_entity;
        $entity_label = $entity->label();
        $return[$entity->bundle()][$entity->id()] = $entity_label;
      }
    }
    return $return;
  }

}
